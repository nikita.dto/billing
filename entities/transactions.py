import logging
from time import time
from sqlalchemy import select, and_, or_

from billing.db.schema import transactions_table, accounts_table
from billing.db.schema import Currency
from billing import errors


log = logging.getLogger(__name__)


async def _topup(conn, to_id, currency, amount):
    """ Update @to_id account to increase it's balance by @amount,
        while checking @currency match.
    """
    update_to_query = accounts_table.update()\
        .values(balance=accounts_table.c.balance + amount)\
        .where(and_(
            accounts_table.c.id == to_id,
            accounts_table.c.currency == currency
        ))
    
    res = await conn.execute(update_to_query)
    if res == 'UPDATE 0':
        raise errors.NotFound()


async def _charge(conn, from_id, currency, amount):
    """ Charge @amount of @currency from @from_id account,
        if currency matches and it have enough on it's balance.
    """
    update_from_query = accounts_table.update()\
        .values(balance=accounts_table.c.balance - amount)\
        .where(and_(
            accounts_table.c.id == from_id,
            accounts_table.c.currency == currency,
            accounts_table.c.balance >= amount
        ))
    
    res = await conn.execute(update_from_query)
    if res == 'UPDATE 0':
        logging.error(f'Failed to charge after check: {from_id} {amount} {currency}')
        raise errors.NotFound()


async def _insert_transaction(conn, from_id, to_id, amount):
    """ Inserts a new transaction record.
    """
    save_tr_query = transactions_table.insert()\
        .values(
            to_id=to_id,
            from_id=from_id,
            amount=amount,
        )\
        .returning(transactions_table.c.id)
        
    transaction_id = await conn.fetchval(save_tr_query)
    return transaction_id


async def _check_and_lock(conn, from_id, to_id, currency, amount):
    """ Fetches accounts and ensures that they have matching currencies
        and @from_id balance is >= @amount.
        Sets lock by using SELECT FOR UPDATE.
    """
    get_accounts = select([
        accounts_table.c.id,
        accounts_table.c.balance,
        accounts_table.c.currency,
    ]).where(or_(
        accounts_table.c.id == to_id,
        accounts_table.c.id == from_id,
    ))

    # Clauses "order by" and "for update" are required to avoid deadlocks
    get_accounts = get_accounts.order_by(accounts_table.c.id).with_for_update()

    accounts_list = await conn.fetch(get_accounts)
    accounts = {a['id']: a for a in accounts_list}
    from_acc = accounts.get(from_id)
    to_acc = accounts.get(to_id)

    if not from_acc:
        raise errors.NotFound('Account not found', details={'what': 'from_account', 'id': from_id})

    if not to_acc:
        raise errors.NotFound('Account not found', details={'what': 'to_account', 'id': to_id})

    if from_acc['currency'] != currency.value:
        raise errors.BadRequest(
            f'Currency mismatch. Expected: {currency}. Found: {from_acc["currency"]}',
            details={'what': 'from_currency'}
        )

    if to_acc['currency'] != currency.value:
        raise errors.BadRequest(
            f'Currency mismatch. Expected: {currency}. Found: {to_acc["currency"]}',
            details={'what': 'to_currency'}
        )

    if from_acc['balance'] < amount:
        raise errors.NotEnoughMinerals()


async def topup_account(db, to_id, currency, amount):
    """ Topup account @to_id with @amount of money.
        It must have matching @currency.
    """
    async with db.transaction() as conn:
        # Updating account's balance
        await _topup(conn, to_id, currency, amount)

        # Logging operation
        transaction_id = await _insert_transaction(conn, None, to_id, amount)

    return {'id': transaction_id, 'from': None, 'to': to_id, 'amount': amount}


async def transfer(db, from_id, to_id, currency, amount):
    """ Transfer @amount of money from account @from_id to account @to_id.
    """
    async with db.transaction() as conn:
        # Checking if it is possible to perform the transaction
        await _check_and_lock(conn, from_id, to_id, currency, amount)

        # Updating @from_id account's balance
        await _charge(conn, from_id, currency, amount)

        # Updating @to_id account's balance
        await _topup(conn, to_id, currency, amount)

        # Logging operation
        transaction_id = await _insert_transaction(conn, from_id, to_id, amount)

    return {'id': transaction_id, 'from': from_id, 'to': to_id, 'amount': amount}


async def find_transactions(db, skip, limit):
    """ Find transactions.
    """
    query = select([
        transactions_table.c.id,
        transactions_table.c.from_id,
        transactions_table.c.to_id,
        transactions_table.c.amount,
        transactions_table.c.time,
    ]).limit(limit).offset(skip).order_by(transactions_table.c.id.desc())
    transactions = await db.fetch(query)

    transactions = list(map(dict, transactions))
    return transactions

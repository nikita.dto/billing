import logging
from sqlalchemy import select

from billing.db.schema import accounts_table
from billing.db.schema import Currency
from billing import errors


log = logging.getLogger(__name__)


async def create_account(db, name, currency=Currency.USD):
    """ Create a new account.
    """
    query = accounts_table.insert().values(
        name=name, currency=currency
    ).returning(accounts_table.c.id)
    account_id = await db.fetchval(query)

    return {'id': account_id, 'name': name, 'currency': currency, 'balance': 0}


async def find_accounts(db, skip=0, limit=10):
    """ Find accounts.
    """
    query = select([
        accounts_table.c.id,
        accounts_table.c.name,
        accounts_table.c.balance,
        accounts_table.c.currency,
    ]).limit(limit).offset(skip).order_by(accounts_table.c.id.desc())
    accounts = await db.fetch(query)

    accounts = list(map(dict, accounts))
    return accounts


async def get_account(db, account_id):
    """ Get account info.
    """
    query = select([
        accounts_table.c.id,
        accounts_table.c.name,
        accounts_table.c.balance,
        accounts_table.c.currency,
    ]).where(accounts_table.c.id == account_id)

    account = await db.fetchrow(query)

    if account is None:
        raise errors.NotFound()

    return dict(account)

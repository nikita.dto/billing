FROM python:3.9
# Install requirements as a separate step to optimize future builds
COPY ./requirements.txt /app/
WORKDIR /app
RUN pip3 install -r requirements.txt
COPY . ./billing

EXPOSE 8080
CMD ["python3", "-m", "billing"]
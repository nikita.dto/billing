import logging
from functools import partial
from configargparse import Namespace
from aiohttp.web_app import Application
from aiohttp import web

from billing import handlers as hs
from billing.middleware import response_formatter
from billing.db.connection import setup_pg



routes = [
    web.view('/accounts', hs.accounts.AccountsListHandler),
    web.view('/accounts/{account_id}', hs.accounts.AccountHandler),
    web.view('/transactions', hs.transactions.TransactionsListHandler),
]


def make_app(args: Namespace) -> Application:
    app = web.Application(
        middlewares=[
            response_formatter,
        ]
    )

    app.cleanup_ctx.append(partial(setup_pg, args=args))

    app.add_routes(routes)

    return app

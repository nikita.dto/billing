import os
import logging
from configargparse import ArgumentParser
import uvloop
import asyncio
from yarl import URL
from aiohttp import web
from aiomisc.log import LogFormat, basic_config

from .app import make_app


log = logging.getLogger(__name__)


parser = ArgumentParser()
parser.add_argument(
    '--ip',
    default='0.0.0.0',
    help='An IP address to listen from',
    env_var='BILLING_IP'
)
parser.add_argument(
    '--port',
    default=8080,
    type=int,
    help='A port to listen',
    env_var='BILLING_PORT'
)
parser.add_argument(
    '--db',
    default='postgresql://billing:billing@localhost/billing',
    help='Postgres connection string',
    env_var='BILLING_DB',
    type=URL
)
parser.add_argument(
    '--pg-pool-min-size', type=int, default=10,
    help='Minimum database connections'
)
parser.add_argument(
    '--pg-pool-max-size', type=int, default=10,
    help='Maximum database connections'
)
parser.add_argument(
    '--log-level',
    default='info',
    choices=('debug', 'info', 'warning', 'error', 'fatal'),
    env_var='BILLING_LOG_LEVEL'
)
parser.add_argument(
    '--log-format',
    choices=LogFormat.choices(),
    default='color',
    env_var='BILLING_LOG_FORMAT'
)


def start():
    args = parser.parse_args()

    # Clear ENV to exclude possibility to steal it
    os.environ.clear()

    # Using additional thread to avoid blocking main thread and ioloop
    # while writing logs
    basic_config(args.log_level, args.log_format, buffered=True)

    # Use uvloop to increase RPS
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

    app = make_app(args)

    log.info(f'Starting server on {args.ip}:{args.port}')
    web.run_app(app, host=args.ip, port=args.port)


if __name__ == '__main__':
    start()

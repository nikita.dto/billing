import logging
import os

from aiohttp.web_app import Application
from configargparse import Namespace
from asyncpgsa import PG
from sqlalchemy import Numeric, cast, func
from sqlalchemy.sql import Select

from .schema import metadata


log = logging.getLogger(__name__)


async def setup_pg(app: Application, args: Namespace) -> PG:
    db_info = args.db.with_password('***')
    log.info('Connecting to database: %s', db_info)

    app['pg'] = PG()
    await app['pg'].init(
        str(args.db),
        min_size=args.pg_pool_min_size,
        max_size=args.pg_pool_max_size
    )
    await app['pg'].fetchval('SELECT 1')

    log.info('Connected to database %s', db_info)

    try:
        yield
    finally:
        log.info('Disconnecting from database %s', db_info)
        await app['pg'].pool.close()
        log.info('Disconnected from database %s', db_info)

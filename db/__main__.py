import os
import logging
from alembic.config import CommandLine, Config

def main():
    alembic = CommandLine()
    alembic.parser.add_argument(
        '--db',
        default=os.getenv('BILLING_DB', ''),
        help='Postgres connection string'
    )
    args = alembic.parser.parse_args()

    config = Config(
        file_=args.config,
        ini_section=args.name,
        cmd_opts=args
    )

    config.set_main_option('sqlalchemy.url', args.db)

    exit(alembic.run_cmd(config, args))


if __name__ == '__main__':
    main()

import enum
from sqlalchemy import (
    MetaData, Table, Column,
    BigInteger, String, Enum, ForeignKey, TIMESTAMP, func
)


metadata = MetaData()


@enum.unique
class Currency(enum.Enum):
    USD = 'USD'


accounts_table = Table(
    'accounts',
    metadata,
    Column('id', BigInteger, primary_key=True),
    Column('name', String, nullable=False),
    Column('currency', Enum(Currency, name='currency'), nullable=False, default=Currency.USD),
    Column('balance', BigInteger, nullable=False, default=0),
)


transactions_table = Table(
    'transactions',
    metadata,
    Column('id', BigInteger, primary_key=True),
    Column('amount', BigInteger),
    Column('to_id', BigInteger, ForeignKey('accounts.id')),
    Column('from_id', BigInteger, ForeignKey('accounts.id'), nullable=True),
    Column('time', TIMESTAMP, server_default=func.now()),
)

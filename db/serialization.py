from enum import Enum
from json import JSONEncoder
from datetime import datetime

from .schema import Currency



class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Enum):
            return obj.value
        
        if isinstance(obj, datetime):
            return str(obj)

        return JSONEncoder.default(self, obj)

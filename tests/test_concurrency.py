import os
import pytest
import random
from multiprocessing import Process
from functools import partial

from .client import APIClient


@pytest.fixture(scope='session')
def api():
    api_url = os.environ['API_URL']
    client = APIClient(api_url)
    return client


def do_procs(procs):
    for proc in procs:
        proc.start()
    
    for proc in procs:
        proc.join()


def scatter_ops(func, op_count, proc_amount):
    """ Scatter @op_count operations between @proc_amount processes.
        Calls @func in separate process with the amount of operations
        that it should perform.
    """
    return [
        Process(target=partial(
            func,
            (op_count // proc_amount) + (1 if i < (op_count % proc_amount) else 0)
        ))
        for i in range(proc_amount)
    ]



def test_concurrent_topup(api):
    """ Performs a batch of topups for a single account concurrently.
    """
    acc = api.create_account('Foo')
    # Amount of money for each transaction
    AMOUNT = 7
    # Amount of operations
    OP_COUNT = 1000
    # Amount of concurrent processes
    CONCURRENCY = 10

    def task(op_count):
        for i in range(op_count):
            api.topup_account(acc['id'], 'USD', AMOUNT)

    procs = scatter_ops(task, OP_COUNT, CONCURRENCY)
    do_procs(procs)

    new_acc = api.get_account(acc['id'])
    assert new_acc['balance'] == OP_COUNT * AMOUNT


def test_concurrent_transfer(api):
    """ Run concurrent money transfer between two accounts in both directions.
    """
    # Initial amount of money for both accounts
    INITIAL_AMOUNT = 1000000
    # Amount to transfer from 1 to 2
    AMOUNT_1_2 = 3
    # Amount to transfer from 2 to 1
    AMOUNT_2_1 = 5
    # Amount of operations in the each direction
    HALF_OP_COUNT = 3000
    # Amount of processes for each of directions
    HALF_CONCURRENCY = 10

    # Prepare accounts
    acc1 = api.create_account('Foo')
    acc2 = api.create_account('Bar')
    api.topup_account(acc1['id'], 'USD', INITIAL_AMOUNT)
    api.topup_account(acc2['id'], 'USD', INITIAL_AMOUNT)

    # Run simultaneous transfer processes in both directions
    def batch(from_id, to_id, amount, op_count):
        for _ in range(op_count):
            api.transfer(from_id, to_id, 'USD', amount)

    procs_1_2 = scatter_ops(
        partial(batch, acc1['id'], acc2['id'], AMOUNT_1_2),
        HALF_OP_COUNT,
        HALF_CONCURRENCY
    )
    procs_2_1 = scatter_ops(
        partial(batch, acc2['id'], acc1['id'], AMOUNT_2_1),
        HALF_OP_COUNT,
        HALF_CONCURRENCY
    )
    
    do_procs(procs_1_2 + procs_2_1)

    # Compare actual and expected balances
    new_acc1 = api.get_account(acc1['id'])
    new_acc2 = api.get_account(acc2['id'])

    assert new_acc1['balance'] == INITIAL_AMOUNT + (AMOUNT_2_1 - AMOUNT_1_2)*HALF_OP_COUNT
    assert new_acc2['balance'] == INITIAL_AMOUNT + (AMOUNT_1_2 - AMOUNT_2_1)*HALF_OP_COUNT

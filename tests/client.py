import pytest
import requests


class APIClient:
    def __init__(self, base_url):
        self.base_url = base_url

    def _req(self, method, url, *args, **kwargs):
        res = requests.request(method, f'{self.base_url}{url}',*args, **kwargs)
        parsed = res.json()
        assert parsed['success'], f'{res} {parsed}'
        return parsed['result']

    def get_account(self, account_id):
        return self._req('get', f'/accounts/{account_id}')
    
    def get_accounts(self, skip=0, limit=10):
        return self._req('get', '/accounts', data={'skip': skip, 'limit': limit})

    def create_account(self, name):
        return self._req('post', '/accounts', json={'name': name})

    def topup_account(self, account_id, currency, amount):
        return self._req(
            'post',
            '/transactions', 
            json={'to': account_id, 'currency': currency, 'amount': amount}
        )

    def transfer(self, from_id, to_id, currency, amount):
        print('Transfer', from_id, to_id, currency, amount)
        return self._req(
            'post',
            '/transactions',
            json={'to': to_id, 'from': from_id, 'currency': currency, 'amount': amount}
        )

    def get_transactions(self, skip=0, limit=10):
        return self._req(
            'get',
            '/transactions',
            data={'skip': skip, 'limit': limit}
        )

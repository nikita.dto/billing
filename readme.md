# Run

`docker-compose up`

# Run tests

`./test.sh`

# API usage examples

## Create account

```
curl -X POST localhost:8080/accounts -d '{"name": "New account"}'
{
  "success": true,
  "result": {
    "id": 178,
    "name": "Acc 1",
    "currency": "USD",
    "balance": 0
  }
}
```

## Topup account

```
curl -X POST localhost:8080/transactions -d '{"to": "178", "amount": 100}' | jq
{
  "success": true,
  "result": {
    "id": 303676,
    "from": null,
    "to": 178,
    "amount": 100
  }
}
```

## Transfer money between accounts
```
curl -X POST localhost:8080/transactions -d '{"from": "178", "to": "177", "amount": 100}' | jq
{
  "success": true,
  "result": {
    "id": 303677,
    "from": 178,
    "to": 177,
    "amount": 100
  }
}
```


# Generate migration

To generate a db creation script substitute POSTGRES_URL and run:

`python3 -m billing.db --db "${POSTGRES_URL}" -c billing/alembic.ini revision --message="Initial" --autogenerate`



class APIError(Exception):
    def __init__(self, code, message=None, details=None):
        self.code = code
        self.message = message
        self.details = details
    

class InternalError(APIError):
    def __init__(self, *args, **kwargs):
        super().__init__('INTERNAL_ERROR', *args, **kwargs)


class BadRequest(APIError):
    def __init__(self, *args, **kwargs):
        super().__init__('BAD_REQUEST', *args, **kwargs)


class MissingArg(BadRequest):
    def __init__(self, name):
        super().__init__(f"Missing argument: '{name}'")


class MalformedArg(BadRequest):
    def __init__(self, name, exp_type):
        super().__init__(f"Malformed argument: '{name}'. Expected type: '{exp_type}'")


class NotFound(APIError):
    def __init__(self, *args, **kwargs):
        super().__init__('NOT_FOUND', *args, **kwargs)


class NotEnoughMinerals(APIError):
    def __init__(self, *args, **kwargs):
        super().__init__('NOT_ENOUGH_MINERALS', *args, **kwargs)

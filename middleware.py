import logging
from aiohttp import web

from .errors import APIError
from .db.serialization import CustomJSONEncoder


log = logging.getLogger(__name__)


def format_result(data=None):
    return {
        'success': True,
        'result': data
    }


def format_error(code, message=None, details=None):
    return {
        'success': False,
        'result': {
            'code': code,
            'message': message,
            'details': details
        }
    }


encoder = CustomJSONEncoder()

@web.middleware
async def response_formatter(request, handler):
    error = None
    try:
        result = await handler(request)
    except APIError as exc:
        error = format_error(exc.code, exc.message, exc.details)
        if exc.code == 'INTERNAL_ERROR':
            log.error(f'Internal error Exception: {error}', exc_info=True)

    except web.HTTPException as exc:
        if exc.status_code in [404, 405]:
            error = format_error('METHOD_NOT_FOUND', 'No such method')
        else:
            log.error(f'web.HTTPException: {exc.code} {exc}', exc_info=True)
            error = format_error('INTERNAL_ERROR', exc.reason)

    except Exception as exc:
        log.error(f'Unknown Exception: {exc}', exc_info=True)
        error = format_error('INTERNAL_ERROR',)

    if error:
        return web.json_response(error)

    return web.json_response(format_result(result), dumps=encoder.encode)

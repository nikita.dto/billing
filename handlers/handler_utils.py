import logging
import json
from aiohttp import web
from functools import wraps

from billing import errors


def parse_json_body(method):
    """ A decorator that performs parsing of json body for handlers.
    """
    @wraps(method)
    async def wrapper(self, *args, **kwargs):
        try:
            self.json_data = await self.request.json()
        except json.decoder.JSONDecodeError as exc:
            raise errors.BadRequest('JSON body expected')

        return await method(self, *args, **kwargs)

    return wrapper


class BasicHandlerMetaclass(type(web.View)):
    """ A constructor class to automatically decorate handlers.
    """
    def __new__(cls, cls_name, bases, dct):
        handler_class = super().__new__(
            cls, cls_name, bases, dct
        )
        for attr_name in dct:
            if attr_name not in ['post', 'put']:
                continue

            method = getattr(handler_class, attr_name)
            method = parse_json_body(method)
            setattr(handler_class, attr_name, method)
        
        return handler_class



class BasicHandler(web.View, metaclass=BasicHandlerMetaclass):
    """ A basic class for all API views.
        Provides some additional methods and automatically decorates handlers,
        using metaclass.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.json_data = {}

    def _get_arg(self, value, name, **kwargs):
        if value is None:
            if 'default' in kwargs:
                return kwargs['default']
            else:
                raise errors.MissingArg(name)
        else:
            arg_type = kwargs.get('type')
            if arg_type:
                try:
                    value = arg_type(value)
                except Exception:
                    raise errors.MalformedArg(name, arg_type)
            
            return value

    def get_body_arg(self, name, **kwargs):
        value = self.json_data.get(name, None)
        return self._get_arg(value, name, **kwargs)
    
    def get_qs_arg(self, name, **kwargs):
        value = self.request.query.get(name, None)
        return self._get_arg(value, name, **kwargs)

    def get_path_arg(self, name, **kwargs):
        value = self.request.match_info.get(name, None)
        return self._get_arg(value, name, **kwargs)

    @property 
    def pg(self):
        return self.request.app['pg']

import logging
from aiohttp import web
import json

from billing import errors
from billing.entities import accounts
from .handler_utils import BasicHandler


log = logging.getLogger(__name__)


class AccountsListHandler(BasicHandler):
    async def post(self):
        """ Create new account.
        """
        name = self.get_body_arg('name')
        account = await accounts.create_account(self.pg, name)

        return account

    async def get(self):
        """ Get accounts.
        """
        skip = self.get_qs_arg('skip', type=int, default=0)
        limit = self.get_qs_arg('limit', type=int, default=10)
        limit = min(limit, 100)
        items = await accounts.find_accounts(self.pg, skip, limit)

        return {'items': items}


class AccountHandler(BasicHandler):
    async def get(self):
        """ Get account info.
        """
        account_id = self.get_path_arg('account_id', type=int)
        account = await accounts.get_account(self.pg, account_id)

        return account

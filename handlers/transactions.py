import logging
from aiohttp import web

from billing import errors
from billing.entities import transactions
from billing.db.schema import Currency
from .handler_utils import BasicHandler


log = logging.getLogger(__name__)


# Max amount allowed for transaction (in cents)
MAX_AMOUNT = 1000000*100


class TransactionsListHandler(BasicHandler):
    async def get(self):
        skip = self.get_qs_arg('skip', type=int, default=0)
        limit = self.get_qs_arg('limit', type=int, default=10)
        limit = min(limit, 100)

        items = await transactions.find_transactions(self.pg, skip, limit)
        return {'items': items}

    async def post(self):
        to_id = self.get_body_arg('to', type=int)
        from_id = self.get_body_arg('from', type=int, default=None)
        amount = self.get_body_arg('amount', type=int)
        if not (0 < amount <= MAX_AMOUNT):
            raise errors.BadRequest(
                f'Amount must be a positive integer <= {MAX_AMOUNT} ',
                details={'what': 'amount'}
            )

        currency = self.get_body_arg('currency', type=Currency, default=Currency.USD)

        if to_id is None:
            raise errors.MissingArg('to')

        if from_id is None:
            # No source account - then it is a topup operation.
            transaction = await transactions.topup_account(self.pg, to_id, currency, amount)
        else:
            transaction = await transactions.transfer(self.pg, from_id, to_id, currency, amount)

        return transaction
